<!DOCTYPE html>
<html>

<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="manifest" href="/manifest.json">
  <meta name="Description" content="UNIPICS" />
  <!-- theme-color defines the top bar color (blue in my case)-->
  <meta name="theme-color" content="#ffffff" />
  <!-- Add to home screen for Safari on iOS-->
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="default" />
  <meta name="apple-mobile-web-app-title" content="Unipics" />
  <link rel="apple-touch-icon" href="img-hc/icon.png" />
  <!-- Add to home screen for Windows-->
  <meta name="msapplication-TileImage" content="img-hc/icon.png" />
  <meta name="msapplication-TileColor" content="#000000" />


  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
  <!-- font awesome CSS -->
  <link rel="stylesheet" href="{{ asset('css/all.css')}}">
  <!-- font awesome CSS -->
  <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
  <!-- swiper CSS -->
  <link rel="stylesheet" href="{{ asset('css/slick.css')}}">
  <link rel="stylesheet" href="{{ asset('css/slick-theme.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css-hc/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css-hc/style-custom.css')}}">
  <link rel="stylesheet" href="{{ asset('css-hc/style-custom-size1.css')}}">
  <link rel="stylesheet" href="{{ asset('css-hc/style-custom-size2.css')}}">
  <link rel="stylesheet" href="{{ asset('css-hc/style-custom-size3.css')}}">
  <link rel="stylesheet" href="{{ asset('css-hc/slicknav.css')}}">
  <!-- style CSS -->


</head>

<body>
  <!-- Preloader Start -->
  <div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
      <div class="preloader-inner position-relative">
        <div class="preloader-circle"></div>
        <div class="preloader-img pere-text">
          {{-- <img src="assets/img/logo/logo.png" alt="" /> --}}
        </div>
      </div>
    </div>
  </div>
  <!-- Preloader Start -->

  <header>
    <!-- Header Start -->
    <div class="header-area header-transparrent">
      <div class="main-header header-sticky">
        <div class="container">
          <div class="row align-items-center">
            <!-- Logo -->
            <div class="col-xl-2 col-lg-2 col-md-2">
              <div class="logo">
                <a href="#"><img src="img-hc/logo.png" class="custom-logo" alt="" /></a>
              </div>
            </div>
            <div class="col-xl-10 col-lg-10 col-md-10 pr-0">
              <!-- Main-menu -->
              <div class="main-menu d-none d-lg-block custom-ml-navbar pr-0">
                <nav class="text-right">
                  <ul id="navigation">
                    <li class="active"><a href="#" class="custom-text-navbar pr-0">ABOUT</a></li>
                    <li><a href="#" class="custom-text-navbar pr-0">WHAT WE DO</a></li>
                    <li>
                      <a href="#" class="custom-text-navbar pr-0">PROJECTS</a>
                      <ul class="submenu">
                        <li><a href="#">Project1</a></li>
                        <li><a href="#">Project2</a></li>
                        <li><a href="#">Project3</a></li>
                      </ul>
                    </li>
                    <li><a href="#" class="custom-text-navbar">CUSTOMER STORIES</a></li>
                  </ul>
                </nav>
              </div>
            </div>
            <!-- Mobile Menu -->
            <div class="col-12">
              <div class="mobile_menu d-block d-lg-none"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Header End -->
  </header>
  @yield('content')
  <!-- Header part end-->

  @include('_footer')