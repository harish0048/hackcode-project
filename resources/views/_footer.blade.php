<!-- Footer part start -->
<footer>
  <hr style="height: 1px; background-color: #C4C4C4; width: 100%">
  <div class="container" style="margin-top: 10px; margin-bottom: 50px">
    <div class="row">
      <div class="col-lg-5" style="margin-top: -5px">
        <h3>Hackcode.id</h3>
        <div class="row" style="margin-top: 95px;">
          <a href="#">
            <img src="img-hc/instagram.png">
          </a>
          <a href="#">
            <img src="img-hc/fb.png">
          </a>
          <a href="#">
            <img src="img-hc/linkedin.png">
          </a>
          <a href="#">
            <img src="img-hc/wa.png">
          </a>

        </div>
      </div>
      <div class="col-lg-5">
        <div style="display: flex">
          <div style="flex: 33%">
            <a href="#">
              <p>Hackcode</p>
            </a>
            <a href="#">
              <p>Hackcode</p>
            </a>
          </div>
          <div style="flex: 33%">
            <a href="#">
              <p>Hackcode</p>
            </a>
            <a href="#">
              <p>Hackcode</p>
            </a>
            <a href="#">
              <p>Hackcode</p>
            </a>
            <a href="#">
              <p>Hackcode</p>
            </a>
          </div>
          <div style="flex: 33%">
            <a href="#">
              <p>Hackcode</p>
            </a>
            <a href="#">
              <p>Hackcode</p>
            </a>
          </div>
        </div>
      </div>
      <div class="col-lg-2">

      </div>
    </div>
  </div>

</footer>
<!-- Footer part end -->

<!-- All JS Custom Plugins Link Here here -->
<script src="js-hc/vendor/modernizr-3.5.0.min.js"></script>

<!-- Jquery, Popper, Bootstrap -->
<script src="js-hc/vendor/jquery-1.12.4.min.js"></script>
<script src="js-hc/popper.min.js"></script>
<script src="js-hc/bootstrap.min.js"></script>
<!-- Jquery Mobile Menu -->
<script src="js-hc/jquery.slicknav.min.js"></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="js-hc/owl.carousel.min.js"></script>
<script src="js-hc/slick.min.js"></script>
<script src="js-hc/carousel.js"></script>
<!-- Date Picker -->
<script src="js-hc/gijgo.min.js"></script>
<!-- One Page, Animated-HeadLin -->
<script src="js-hc/wow.min.js"></script>
<script src="js-hc/animated.headline.js"></script>
<script src="js-hc/jquery.magnific-popup.js"></script>

<!-- Scrollup, nice-select, sticky -->
<script src="js-hc/jquery.scrollUp.min.js"></script>
<script src="js-hc/jquery.nice-select.min.js"></script>
<script src="js-hc/jquery.sticky.js"></script>

<!-- contact js -->
<script src="js-hc/contact.js"></script>
<script src="js-hc/jquery.form.js"></script>
<script src="js-hc/jquery.validate.min.js"></script>
<script src="js-hc/mail-script.js"></script>
<script src="js-hc/jquery.ajaxchimp.min.js"></script>

<!-- Jquery Plugins, main Jquery -->
<script src="js-hc/plugins.js"></script>
<script src="js-hc/main.js"></script>

<script>
  if ('serviceWorker' in navigator) {
  // Use the window load event to keep the page load performant
  window.addEventListener('load', () => {
  navigator.serviceWorker.register('/service-worker.js');
  });
  }
</script>


</body>

</html>