@extends('_header')

@section('title', 'Get in Touch')

@section('content')
<!-- Content part start -->
<div style="width: 100%; margin-bottom: 500px; margin-top: 100px">
  <div class="text-center">
    <img src="img-hc/get-in-touch.png" style="width: 43%">
    <div style="width: 100%; margin-top: -575px">
      <div class="text-center">
        <div style="width: 375px; height: 300px; margin: 0 auto">
          <h3 style="text-align: left; font-size: 36px; font-weight: bold; color: black">Let's talk!</h3>
          <p style="text-align: left; font-size: 14px; color: black">We will get back to you within one to two days
            through email.
            Also please don’t
            forget to check your spam
            account just
            in case!</p>
          <form style="position: relative">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Name*"
                style="border:hidden; border-bottom: 1px solid #C4C4C4">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Email*"
                style="border:hidden; border-bottom: 1px solid #C4C4C4">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Subject*"
                style="border:hidden; border-bottom: 1px solid #C4C4C4">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Message*"
                style="border:hidden; border-bottom: 1px solid #C4C4C4">
            </div>
          </form>
          <div class="single_sidebar text-left">
            <a href="#" class="button-request">
              <h4 class="custom-text-request">SEND REQUEST</h4>
            </a>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>

<!-- Content part end -->
@endsection