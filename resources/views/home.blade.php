@extends('_header')

@section('title', 'UNIPICS')

@section('content')
<!-- Content part start -->
<div style="overflow: hidden">
  <div class="custom-div-bg-img">
    <img src="img-hc/hero.png" class="custom-bg-img">
  </div>
  <div class="container">
    <div class="col-lg-6">
      <h1 class="custom-title-header">Product<br>Development<br>Studio</h1>
      <p class="custom-subtitle-header">From apps to artificial intellegence, from brands to blockchain, we'll craft
        delightful product you're looking
        for.</p>
      <div class="single_sidebar">
        <a href="#" class="button-home-work" style="display: flex">
          <h4 class="custom-h4-home custom-text-btn">See Our Project</h4>
          <img src="img-hc/arrow.png" class="custom-arrow">
        </a>
      </div>
    </div>
  </div>
  <div class="container custom-mt-project no-ipad">
    <div class="custom-mb-text-project">
      <h3 class="custom-title-project">Our Project</h3>
      <p class="custom-subtitle-project">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur sequi
        corporis rem harum fugit excepturi
        hic laborum magnam, vitae veritatis quasi nostrum est soluta eligendi aut esse quas necessitatibus. Enim!</p>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <img src="img-hc/bg-project1.png" class="custom-bg-project1">
        <div class="custom-mt-div-project">
          <h3 class="custom-title-project-content">Industry Information for everyone</h3>
          <p>SMARTINDUSTRY.COM</p>
        </div>
        <div class=" text-center">
          <img src="img-hc/project1.png" class="custom-img-project">
        </div>
      </div>
      <div class="col-lg-6">
        <img src="img-hc/bg-project2.png" class="custom-bg-project2">
        <div class="custom-mt-div-project2">
          <h3 class="custom-title-project-content">Industry Information for everyone</h3>
          <p>YPTI.COM</p>
        </div>
        <div class=" text-center">
          <img src="img-hc/project2.png" class="custom-img-project2">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <img src="img-hc/material.png" class="custom-material-project">
        <img src="img-hc/bg-project2.png" class="custom-bg-project3">
        <div class="custom-mt-div-project3">
          <h3 class="custom-title-project-content">Industry Information for everyone</h3>
          <p>YPTI.COM</p>
        </div>
        <div class="row text-center custom-pad-row-project" style="display: flex">
          <div class="custom-pad-content-project" style="flex: 50%">
            <img src="img-hc/project2.png" class="custom-img-project3">
          </div>
          <div class="custom-pad-content-project" style="flex: 50%">
            <img src="img-hc/project2.png" class="custom-img-project4">
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <img src="img-hc/bg-project1.png" class="custom-bg-project4">
        <div class="custom-mt-div-project">
          <h3 class="custom-title-project-content">Industry Information for everyone</h3>
          <p>SMARTINDUSTRY.COM</p>
        </div>
        <div class=" text-center">
          <img src="img-hc/project3.png" class="custom-img-project5">
        </div>
      </div>
    </div>
    <div class="custom-mt-curious">
      <div style="float: right">
        <h3 class="custom-curious-project">Curious for the projects ?</h3>
        <div class="container row custom-col-curious">
          <a href="#">
            <p class="custom-text-curious">Browse our projects</p>
          </a>
          <img src="img-hc/btn-curious.png" class="custom-btn-curious">
        </div>
      </div>
    </div>
  </div>
  <div class="container custom-mt-project ipad">
    <div>
      <h3 class="custom-title-project">Our Project</h3>
      <p class="custom-subtitle-project">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur sequi
        corporis rem harum fugit excepturi
        hic laborum magnam, vitae veritatis quasi nostrum est soluta eligendi aut esse quas necessitatibus. Enim!</p>
    </div>
    <div class="row" style="display: flex">
      <div class="" style="flex: 50%; padding: 10px 10px">
        <img src="img-hc/bg-project1.png" class="custom-bg-project1">
        <div class="custom-mt-div-project">
          <h3 class="custom-title-project-content">Industry Information for everyone</h3>
          <p>SMARTINDUSTRY.COM</p>
        </div>
        <div class=" text-center">
          <img src="img-hc/project1.png" class="custom-img-project">
        </div>
      </div>
      <div class="" style="flex: 50%; padding: 10px 10px">
        <img src="img-hc/bg-project2.png" class="custom-bg-project2">
        <div class="custom-mt-div-project2">
          <h3 class="custom-title-project-content">Industry Information for everyone</h3>
          <p>YPTI.COM</p>
        </div>
        <div class=" text-center">
          <img src="img-hc/project2.png" class="custom-img-project2">
        </div>
      </div>
    </div>
    <div class="row" style="display: flex">
      <div class="" style="flex: 50%; padding: 10px 10px">
        <img src="img-hc/material.png" class="custom-material-project">
        <img src="img-hc/bg-project2.png" class="custom-bg-project3">
        <div class="custom-mt-div-project3">
          <h3 class="custom-title-project-content">Industry Information for everyone</h3>
          <p>YPTI.COM</p>
        </div>
        <div class="row text-center custom-pad-row-project" style="display: flex">
          <div class="custom-pad-content-project" style="flex: 50%">
            <img src="img-hc/project2.png" class="custom-img-project3">
          </div>
          <div class="custom-pad-content-project" style="flex: 50%">
            <img src="img-hc/project2.png" class="custom-img-project4">
          </div>
        </div>
      </div>
      <div class="" style="flex: 50%; padding: 10px 10px">
        <img src="img-hc/bg-project1.png" class="custom-bg-project4">
        <div class="custom-mt-div-project">
          <h3 class="custom-title-project-content">Industry Information for everyone</h3>
          <p>SMARTINDUSTRY.COM</p>
        </div>
        <div class=" text-center">
          <img src="img-hc/project3.png" class="custom-img-project5">
        </div>
      </div>
    </div>
    <div class="custom-mt-curious">
      <div style="float: right">
        <h3 class="custom-curious-project">Curious for the projects ?</h3>
        <div class="container row custom-col-curious">
          <a href="#">
            <p class="custom-text-curious">Browse our projects</p>
          </a>
          <img src="img-hc/btn-curious.png" class="custom-btn-curious">
        </div>
      </div>
    </div>
  </div>
  <div class="container custom-mt-service no-ipad">
    <img src="img-hc/bg-service.png" class="custom2-bg-img-service">
    <div class="custom-mt-card">
      <h2 class="custom-title-project">Our Service</h2>
      <div class="row">
        <div class="col-lg-4 custom-pad-service">
          <img src="img-hc/card-service.png" class="custom-card-service">
          <div class="custom-mt-text-card">
            <h3 class="custom-title-card">Project Devision</h3>
            <p class="custom-subtitle-card">Website<br>Web Based Application<br>Mobile Application</p>
          </div>
        </div>
        <div class="col-lg-4 custom-pad-service">
          <img src="img-hc/card-service.png" class="custom-card-service">
          <div class="custom-mt-text-card">
            <h3 class="custom-title-card">Training</h3>
            <p class="custom-subtitle-card">IT Seminar<br>Workshop<br>Web Development Training</p>
          </div>
        </div>
        <div class="col-lg-4 custom-pad-service">
          <img src="img-hc/card-service.png" class="custom-card-service">
          <div class="custom-mt-text-card">
            <h3 class="custom-title-card">Consultancy</h3>
            <p class="custom-subtitle-card">Bimbingan Teknis<br>Konsultasi Teknis<br>Analisa Teknis</p>
          </div>
        </div>
      </div>

    </div>
  </div>
  <div class="container custom-mt-service ipad">
    <img src="img-hc/bg-service.png" class="custom2-bg-img-service">
    <div class="custom-mt-card">
      <h2 class="custom-title-project">Our Service</h2>
      <div class="row">
        <div class="col-lg-4 custom-pad-service text-center">
          <img src="img-hc/card-service.png" class="custom-card-service">
          <div class="custom-mt-text-card text-left">
            <h3 class="custom-title-card">Project Devision</h3>
            <p class="custom-subtitle-card">Website<br>Web Based Application<br>Mobile Application</p>
          </div>
        </div>
        <div class="col-lg-4 custom-pad-service text-center">
          <img src="img-hc/card-service.png" class="custom-card-service">
          <div class="custom-mt-text-card text-left">
            <h3 class="custom-title-card">Training</h3>
            <p class="custom-subtitle-card">IT Seminar<br>Workshop<br>Web Development Training</p>
          </div>
        </div>
        <div class="col-lg-4 custom-pad-service text-center">
          <img src="img-hc/card-service.png" class="custom-card-service">
          <div class="custom-mt-text-card text-left">
            <h3 class="custom-title-card">Consultancy</h3>
            <p class="custom-subtitle-card">Bimbingan Teknis<br>Konsultasi Teknis<br>Analisa Teknis</p>
          </div>
        </div>
      </div>

    </div>
  </div>
  <div class="container custom-mt-client">
    <div class="row">
      <div class="text-center">
        <h3 class="custom-title-project">Our Client</h3>
        <p class="custom-subtitle-project">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur sequi
          corporis rem harum fugit excepturi
          hic laborum magnam, vitae veritatis quasi nostrum est soluta eligendi aut esse quas necessitatibus. Enim!</p>
      </div>
      <div class="client row w-100 custom-ml-client">
        <div class="col-lg-3">
          <img src="img-hc/client1.png" class="custom-img-client">
        </div>
        <div class="col-lg-3">
          <img src="img-hc/client2.png" class="custom-img-client">
        </div>
        <div class="col-lg-3">
          <img src="img-hc/client3.png" class="custom-img-client">
        </div>
        <div class="col-lg-3">
          <img src="img-hc/client4.png" class="custom-img-client">
        </div>
        <div class="col-lg-3">
          <img src="img-hc/client5.png" class="custom-img-client">
        </div>
        <div class="col-lg-3">
          <img src="img-hc/client3.png" class="custom-img-client">
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="custom-mt-testi">
      <img src="img-hc/card-ex.png" class="custom-card-testi1">
      <img src="img-hc/card-ex.png" class="custom-card-testi2">
      <img src="img-hc/card-ex.png" class="custom-card-testi3">
      <img src="img-hc/card-ex.png" class="custom-card-testi4">
      <img src="img-hc/card-ex.png" class="custom-card-testi5">
    </div>
    <div class="row custom-mt-testi-slider" style="margin-bottom: 100px">
      <div class="col-lg-5">
        <div class="testi-nav-thumbnails">
          <div>
            <div class="card custom-card-testi-slider">
              <img src="img-hc/client.png" class="custom-img-card-slider">
              <div class="card-body">
                <p class="custom-p-testi-slider">Lorem ipsum dolor sit amet
                  consectetur
                  adipisicing elit.
                  Odio enim
                  aspernatur, voluptatibus dicta in,
                  itaque doloribus eveniet exercitationem veniam eos dolorem ratione at inventore quas ipsa! Enim,
                  dolore? Praesentium, quasi?</p>
                <hr class="custom-hr-slider">
                <p class="custom-name-card">
                  Harish Setyo Hudnanto
                </p>
                <p class="custom-name-title-card">Web Developer</p>
              </div>
            </div>
          </div>
          <div>
            <div class="card custom-card-testi-slider">
              <img src="img-hc/author.png" class="custom-img-card-slider">
              <div class="card-body">
                <p class="custom-p-testi-slider">Lorem ipsum dolor sit amet
                  consectetur
                  adipisicing elit.
                  Odio enim
                  aspernatur, voluptatibus dicta in,
                  itaque doloribus eveniet exercitationem veniam eos dolorem ratione at inventore quas ipsa! Enim,
                  dolore? Praesentium, quasi?</p>
                <hr class="custom-hr-slider">
                <p class="custom-name-card">
                  Wahyudi
                </p>
                <p class="custom-name-title-card">Web Developer</p>
              </div>
            </div>
          </div>
          <div>
            <div class="card custom-card-testi-slider">
              <img src="img-hc/client.png" class="custom-img-card-slider">
              <div class="card-body">
                <p class="custom-p-testi-slider">Lorem ipsum dolor sit amet
                  consectetur
                  adipisicing elit.
                  Odio enim
                  aspernatur, voluptatibus dicta in,
                  itaque doloribus eveniet exercitationem veniam eos dolorem ratione at inventore quas ipsa! Enim,
                  dolore? Praesentium, quasi?</p>
                <hr class="custom-hr-slider">
                <p class="custom-name-card">
                  Harish Setyo Hudnanto
                </p>
                <p class="custom-name-title-card">Web Developer</p>
              </div>
            </div>
          </div>
          <div>
            <div class="card custom-card-testi-slider">
              <img src="img-hc/author.png" class="custom-img-card-slider">
              <div class="card-body">
                <p class="custom-p-testi-slider">Lorem ipsum dolor sit amet
                  consectetur
                  adipisicing elit.
                  Odio enim
                  aspernatur, voluptatibus dicta in,
                  itaque doloribus eveniet exercitationem veniam eos dolorem ratione at inventore quas ipsa! Enim,
                  dolore? Praesentium, quasi?</p>
                <hr class="custom-hr-slider">
                <p class="custom-name-card">
                  Wahyudi
                </p>
                <p class="custom-name-title-card">Web Developer</p>
              </div>
            </div>
          </div>
          <div>
            <div class="card custom-card-testi-slider">
              <img src="img-hc/client.png" class="custom-img-card-slider">
              <div class="card-body">
                <p class="custom-p-testi-slider">Lorem ipsum dolor sit amet
                  consectetur
                  adipisicing elit.
                  Odio enim
                  aspernatur, voluptatibus dicta in,
                  itaque doloribus eveniet exercitationem veniam eos dolorem ratione at inventore quas ipsa! Enim,
                  dolore? Praesentium, quasi?</p>
                <hr class="custom-hr-slider">
                <p class="custom-name-card">
                  Harish Setyo Hudnanto
                </p>
                <p class="custom-name-title-card">Web Developer</p>
              </div>
            </div>
          </div>
          <div>
            <div class="card custom-card-testi-slider">
              <img src="img-hc/author.png" class="custom-img-card-slider">
              <div class="card-body">
                <p class="custom-p-testi-slider">Lorem ipsum dolor sit amet
                  consectetur
                  adipisicing elit.
                  Odio enim
                  aspernatur, voluptatibus dicta in,
                  itaque doloribus eveniet exercitationem veniam eos dolorem ratione at inventore quas ipsa! Enim,
                  dolore? Praesentium, quasi?</p>
                <hr class="custom-hr-slider">
                <p class="custom-name-card">
                  Wahyudi
                </p>
                <p class="custom-name-title-card">Web Developer</p>
              </div>
            </div>
          </div>
        </div>
        <img src="img-hc/icon-semicolon.png" class="custom-icon1">
        <img src="img-hc/icon-testi.png" class="custom-icon2">
      </div>
      <div class="col-lg-7">
        <div class="testi row w-100">
          <div>
            <img src="img-hc/author.png" class="custom-img-testi">
            <p class="custom-p-testi">Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit eum hic incidunt
              saepe culpa ex minima
              corporis nesciunt laudantium, reiciendis cumque quia expedita pariatur deleniti, dolorem vero quaerat
              ducimus recusandae.</p>
            <p class="custom-name-testi">WAHYUDI</p>
            <p class="custom-jobtitle-testi">Founder Lalalili</p>
          </div>
          <div>
            <img src="img-hc/client.png" class="custom-img-testi">
            <p class="custom-p-testi">Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit eum hic incidunt
              saepe culpa ex minima
              corporis nesciunt laudantium, reiciendis cumque quia expedita pariatur deleniti, dolorem vero quaerat
              ducimus recusandae.</p>
            <p class="custom-name-testi">HARISH SETYO HUDNANTO</p>
            <p class="custom-jobtitle-testi">Founder Lalalili</p>
          </div>
          <div>
            <img src="img-hc/author.png" class="custom-img-testi">
            <p class="custom-p-testi">Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit eum hic incidunt
              saepe culpa ex minima
              corporis nesciunt laudantium, reiciendis cumque quia expedita pariatur deleniti, dolorem vero quaerat
              ducimus recusandae.</p>
            <p class="custom-name-testi">WAHYUDI</p>
            <p class="custom-jobtitle-testi">Founder Lalalili</p>
          </div>
          <div>
            <img src="img-hc/client.png" class="custom-img-testi">
            <p class="custom-p-testi">Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit eum hic incidunt
              saepe culpa ex minima
              corporis nesciunt laudantium, reiciendis cumque quia expedita pariatur deleniti, dolorem vero quaerat
              ducimus recusandae.</p>
            <p class="custom-name-testi">HARISH SETYO HUDNANTO</p>
            <p class="custom-jobtitle-testi">Founder Lalalili</p>
          </div>
          <div>
            <img src="img-hc/author.png" class="custom-img-testi">
            <p class="custom-p-testi">Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit eum hic incidunt
              saepe culpa ex minima
              corporis nesciunt laudantium, reiciendis cumque quia expedita pariatur deleniti, dolorem vero quaerat
              ducimus recusandae.</p>
            <p class="custom-name-testi">WAHYUDI</p>
            <p class="custom-jobtitle-testi">Founder Lalalili</p>
          </div>
          <div>
            <img src="img-hc/client.png" class="custom-img-testi">
            <p class="custom-p-testi">Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit eum hic incidunt
              saepe culpa ex minima
              corporis nesciunt laudantium, reiciendis cumque quia expedita pariatur deleniti, dolorem vero quaerat
              ducimus recusandae.</p>
            <p class="custom-name-testi">HARISH SETYO HUDNANTO</p>
            <p class="custom-jobtitle-testi">Founder Lalalili</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="custom-touch">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="custom-margin-text-touch">
            <h3 class="custom-title-touch">Not sure where to start?</h3>
            <p class="custom-subtitle-touch">We'll help you figure it out</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="row custom-margin-touch">
            <div>
              <a href="{{ route('getintouch') }}">
                <p class="custom-p-touch">Get in touch</p>
              </a>
            </div>
            <div>
              <img src="img-hc/btn-touch.png" class="custom-btn-touch">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container custom-mt-culture">
    <div class="row">
      <div style="display: flex">
        <div class="custom-mt-text-culture" style="flex: 50%">
          <h3 class="custom-title-culture">Life and Culture</h3>
          <p class="custom-subtitle-culture">We are horizontal company, and communication and transparency are important
            to us. We value work, life
            balance, and this
            enables us to go the extra mile for our clients</p>
          <div class="container row">
            <a href="#">
              <p class="custom-text-curious">Find out more</p>
            </a>
            <img src="img-hc/btn-curious.png" class="custom-btn-curious">
          </div>
        </div>
        <div style="flex: 50%">
          <img src="img-hc/ilustrasi.png" class="custom-img-culture">
        </div>
      </div>
    </div>
  </div>

</div>

<!-- Content part end -->
@endsection