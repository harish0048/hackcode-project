module.exports = {
    globDirectory: 'public/',
    globPatterns: ['**/*.{js,css,png,jpg}', 'offline.html'],
    swDest: 'public/service-worker.js',
    runtimeCaching: [
        {
            urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
            handler: 'NetworkFirst',
            options: {
                cacheName: 'images',
                expiration: {
                    maxEntries: 10,
                },
            },
        },
    ],
};
