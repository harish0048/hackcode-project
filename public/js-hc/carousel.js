$('.client').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    // vertical: true,
    // verticalSwiping: true,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 1000,
    responsive: [
        {
            breakpoint: 1024,
            settings: 'unslick',
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            },
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                nextArrow: false,
                centerMode: false,
                focusOnSelect: false,
            },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ],
});

$('.testi').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    autoplay: false,
    draggable: false,
    swipe: false,
    asNavFor: '.testi-nav-thumbnails',
});

$('.testi-nav-thumbnails').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.testi',
    dots: false,
    focusOnSelect: false,
    vertical: true,
    verticalSwiping: true,
    autoplay: true,
    autoplaySpeed: 3500,
});

// Remove active class from all thumbnail slides
$('.testi-nav-thumbnails .slick-slide').removeClass('slick-active');

// Set active class to first thumbnail slides
$('.testi-nav-thumbnails .slick-slide')
    .eq(0)
    .addClass('slick-active');

// On before slide change match active thumbnail to current slide
$('.testi').on('beforeChange', function (
    event,
    slick,
    currentSlide,
    nextSlide,
) {
    var mySlideNumber = nextSlide;
    $('.testi-nav-thumbnails .slick-slide').removeClass(
        'slick-active',
    );
    $('.testi-nav-thumbnails .slick-slide')
        .eq(mySlideNumber)
        .addClass('slick-active');
});
